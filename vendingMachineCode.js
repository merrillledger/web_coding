readline = require('readline-sync'); /*This activates the readline function so we can get a userinput*/

function menuitem(){   /*This creates a class for the different menu items*/
      this.letter = ''; /*Adds letter attribute to the class*/
      this.name = ''; /*Adds name attribute to the class*/
      this.price = 0; /*Adds a price attribute*/
    }

menuitem.prototype.printValues = function(){ /*This creates a prototype called printValues*/
      return this.letter + '.' + ' ' + this.name + '    ' + '£' + this.price; /*This allows me to call the values from the objects without having to retype them. This is created to print out the menu items in the wlecomeMenu*/
    };

menuitem.prototype.ifstatement = function(){ /*This creates a prototype called ifstatements*/
        if(usercredit >= this.price){ /*If the users credit is more than or equal to the cost of the item selected*/
            console.log('Your product' + ' ' + this.letter + '.' +' '+ this.name +' '+'dispensed'); /*This tells the user their snack has been dispensed*/
            usercredit = usercredit - this.price; /*This removes the price of the item selected from the usercredit*/
            console.log('£' + usercredit.toPrecision(3)); /*Prints the new usercredit balance and uses precision for decimal places*/
          } else{ /*If the user doesnt have the funds*/
            console.log('Insufficent funds, please insert more money!') /*Print a message saying the user doesnt have the funds*/
            addingcredit(); /*allows the user to add more credit*/
            buyingproduct(); /*When they have added more criedt it allows them to try and buy the product again*/
          }
        };

  var wispa = new menuitem(); /*Adds a new object to the menu item class*/
  wispa.letter = 'a'; /*Sets the wispa letter to a*/
  wispa.name = 'Wispa'; /*Sets the name to Wispa*/
  wispa.price = 1.10; /*sets the price to £1.10*/


  var twix = new menuitem() /*Adds a new object to the menu item class*/
  twix.letter = 'b'; /*Sets the twix letter to b*/
  twix.name = 'Twix'; /*Sets the twix name to Twix*/
  twix.price = 1.40; /*sets the price to £1.40*/

  var aero = new menuitem() /*Adds a new object to the menu item class*/
  aero.letter = 'c'; /*Sets the aero letter to c*/
  aero.name = 'Areo'; /*Sets the aero name to Aero*/
  aero.price = 1.20; /*sets the price to £1.20*/


  var mentos = new menuitem() /*Adds a new object to the menu item class*/
  mentos.letter = 'd'; /*Sets the mentos letter to d*/
  mentos.name = 'Mentos';/*Sets the mentos name to Mentos*/
  mentos.price = 2.00; /*sets the price to £2.00*/


  var crisps = new menuitem() /*Adds a new object to the menu item class*/
  crisps.letter = 'e'; /*Sets the crisps letter to e*/
  crisps.name = 'Crisps'; /*Sets the crisps name to Crisps*/
  crisps.price = 1.00; /*sets the price to £1.00*/

  var usercredit = 0; /*creates the user credit variable and sets the user credit to 0*/

function welcomeMenu(){ /*This gives an overall layout for the user view*/
    console.log('        +--------------- +'); /*The menu and the prices for the user*/
    console.log('        |Snacks    Prices|'); /*Titles for the products and prices*/
    console.log('        |' + wispa.printValues() +'|'); /*Retrieves the code from printValues and applys it to the wispa object*/
    console.log('        |' + twix.printValues() + ' |');/*Retrieves the code from printValues and applys it to the twix object*/
    console.log('        |' + aero.printValues() + ' |'); /*Retrieves the code from printValues and applys it to the aero object*/
    console.log('        |' + mentos.printValues() + ' |'); /*Retrieves the code from printValues and applys it to the mentos object*/
    console.log('        |' + crisps.printValues() + ' |'); /*Retrieves the code from printValues and applys it to the crisps object*/
    console.log('+-------+----------------+-------+');
    console.log('|--------Vending Machine!--------|');/*Greetings message*/
    console.log('+--------------------------------+');
    console.log('Current balance is:' + '£' + usercredit.toPrecision(3));/*Shows the users current balance*/
    console.log('+--------------------------------+');
    console.log(addingcredit());  /*This calls the adding credit function so the user can add credit*/
    console.log('+--------------------------------+');
    console.log(buyingproduct());   /*This calls the buyingproduct function so the user can add purchase a product*/
    console.log('+--------------------------------+');
    console.log(refundingcredit(['Yes', 'No']));  /*This calls the refunding credit function so the user can get their credit back or chose to buy another product*/
    console.log('+--------------------------------+');
}

function addingcredit(userinput){ /*This is allowing the user to add credit to the vending machine*/
    var userinput = readline.question('Please insert money: '); /*This allows the user to input an amout into the vending machine*/

    if(userinput >= 0){ /*Having this if statement in place prevents the user from inputting something that isnt a number*/
      usercredit = Number(userinput) + usercredit; /*This adds the userinput to the users current credit balance*/
      console.log('Current balance is now:' + '£' + usercredit.toPrecision(3)); /*Then it prints the users new balance and the toPrecision function adds the decimal places*/
    }else{
      console.log('This is not a number! Please enter a number.')/*Gives the user a messgae telling them that they havent inputted a number*/
      addingcredit(); /*This gives them the a chance to re-enter a number*/
    }
  }


function buyingproduct(meunitems){ /*This allows the user to buy a item from the vending machine and take it off their current balance*/
    var choice = readline.question('Please enter the letter for the snack of your choice: '); /*This alloes the user to choose a snack from the menu*/
    console.log('You chose: ' + choice); /*prints their choice*/

      if(choice == wispa.letter){ /*If the users input matched the letter of wispa(a)*/
          console.log(wispa.ifstatement()); /*This retrieves the code from ifstatements and applys it to the object wispa*/
        } else if(choice == twix.letter){ /*If the users input matched the letter of twix(b)*/
          console.log(twix.ifstatement());/*This retrieves the code from ifstatements and applys it to the object twix*/
        } else if(choice == aero.letter){ /*if the users input matched the letter of aero(c)*/
          console.log(aero.ifstatement()); /*This retrieves the code from ifstatements and applys it to the object aero*/
        } else if(choice == mentos.letter){/*if the users input matched the letter of mentos(d)*/
          console.log(mentos.ifstatement()); /*This retrieves the code from ifstatements and applys it to the object mentos*/
        } else if(choice == crisps.letter){/*if the users input matched the letter of mentos(d)*/
          console.log(crisps.ifstatement());/*This retrieves the code from ifstatements and applys it to the object crisps*/
        } else {
          console.log('ERROR! You havent entered a letter!') /*Message to tell the user that they havent inputted one of the letters for the products*/
          buyingproduct(); /*When they have added more criedt it allows them to try and buy the product again*/
        }
      }



function refundingcredit(){ /*This function refunds the user the credit in the vending machine and allows them to purchase another product*/
      var refund = readline.question('Would you like a refund? Please type Yes or No: '); /*Allows the user to chose if they want a refund or not*/

      if(refund == ('Yes' || 'yes')){ /*If the user says Yes or yes*/
        if(usercredit > 0){ /*If the usercredit id greater than 0*/
           usercredit = 0; /*Sets the usercredit to 0*/
           console.log('You have been refunded the remainder of your money'); /*Tells the user that they have been refunded*/
           console.log('Your current balance is: ' + '£' + usercredit.toPrecision(3)); /*Prints the users credits new value*/
         }else{ /*if the users credit isn't greater than 0*/
           console.log('You havent got the funds to be eligable for a refund')/*Tells the user that they havent got credit for a refund*/
         }
      } else{/*if the user inputs No*/
          var anotherpurchase = readline.question('Would you like to make another purchase? Please type Yes or No:'); /*Allows the user make another purchase*/
          if(anotherpurchase == ('Yes' || 'yes')){ /*If the user types Yes or yes*/
             welcomeMenu(); /*Take the user back to the welcome menu*/
          }else{ /*if the user says no*/
             console.log('Thankyou for vending with us!') /*Thsnkyou message*/
            }
          }
        }

 /*Gives the layour for the interface for the vending machine*/
welcomeMenu()
