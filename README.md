Download this file and open the file using konsole.

If you download from this file from the dropbox, type in the file pathway in the command line - 'cd downloads/Ledger_M_24205044_CW2/app'
If using the link in the report, go to downloads and dowload then type in the file pathway in the command line - 'cd downloads/Ledger_M_24205044_CW2/app'
If using the link the code in the repository can be copied and pasted into your chosen code editior then enter the file pathway that you saved it in using 'cd'

When in the correct pathway you can now run the code,
To run the code in command line type 'node vendingMachineCode.js'

The instructions will be on the screen on your konsole!
The rest of the instructions are also in the report!
